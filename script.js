const gameContainer = document.getElementById("game");

let nameofgif =[
  1,2,3,4,5,6,7,8,9,10,11,12,
  1,2,3,4,5,6,7,8,9,10,11,12
]

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shufflegif = shuffle(nameofgif)

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
  for (let color of colorArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(color);

    const cardFront = document.createElement("img")
    cardFront.src = `./gifs/${color}.png`
    cardFront.classList.add('cardFront')
    // call a function handleCardClick when a div is clicked on

    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

function deleteDivsForColor(){
  while(gameContainer.firstChild){
    gameContainer.removeChild(gameContainer.firstChild)
  }
  shufflegif = shuffle(nameofgif)
}
let bestscore = localStorage.getItem('score')? localStorage.getItem('score') : 0
let lis = []
let noofclicks = 0
let nooftiles = 0;
let cardfliped = []
let twocardflipedcheck = true
// TODO: Implement this function!
function handleCardClick(event) {
  if(twocardflipedcheck){
    document.getElementsByClassName('tap')[0].style.display = 'none'
    document.getElementsByClassName('restart')[0].style.display = 'block'
    let cardcheck = cardfliped.filter((element)=>{
      return element.target === event.target
    })
    if(lis.length === 0 && cardcheck.length===0){
      
      let classname = event.target.className
    // you can use event.target to see which element was clicked
      event.target.classList.add('flip')
      event.target.style.backgroundImage = `url("./gifs/${classname}.png")`
      lis.push(event)
      cardfliped.push(event)
      noofclicks += 1
      nooftiles += 1
    }else{
      let lastevent = lis[0]
      let cardcheck = cardfliped.filter((element)=>{
        return element.target === event.target
      })
      if(cardcheck.length === 0){
        let classname = event.target.className
    // you can use event.target to see which element was clicked
    event.target.classList.add('flip')
    event.target.style.backgroundImage = `url("./gifs/${classname}.png")`
    
        noofclicks += 1
        nooftiles += 1
        lis.pop()
        if(event.target.className === lastevent.target.className && event.target !== lastevent.target){
          cardfliped.push(event)

          if(nooftiles === gameContainer.children.length){
            setTimeout(()=> {
              document.querySelector('.noofclicks1').innerText = `Bestscore = ${bestscore} Score = ${noofclicks}`
              document.getElementsByClassName('gameover')[0].style.display = 'flex'
            },100)
            console.log(localStorage.getItem('score') > noofclicks)
            console.log(localStorage.getItem('score'))
            if(localStorage.getItem('score') === null || localStorage.getItem('score') > noofclicks){
              localStorage.setItem('score',noofclicks)
              bestscore = noofclicks
              console.log(bestscore)
            }
          }
        }else{
          twocardflipedcheck = false
          nooftiles -= 2
          cardfliped.pop(lastevent)
          setTimeout(()=>{
            event.target.style.backgroundImage = 'url("./gifs/card.jpeg")'
            lastevent.target.style.backgroundImage = 'url("./gifs/card.jpeg")'
            twocardflipedcheck = true
            event.target.classList.remove('flip');
            lastevent.target.classList.remove('flip');
          },1000)
        }
      }
    }
    document.querySelector('.noofclicks').innerText = `Bestscore = ${bestscore} Score = ${noofclicks} cards flip = ${nooftiles}`
  }
}

// when the DOM loads
let a = true;


document.getElementsByClassName('start')[0].addEventListener('click',(event)=>{
  if(a){
    document.querySelector('.noofclicks').innerText = `Bestscore = ${bestscore} Score = ${noofclicks} cards flip = ${nooftiles}`
    createDivsForColors(shufflegif)
    a = false
    document.getElementsByTagName('body')[0].style.padding = '0px'
    document.getElementsByClassName('start')[0].style.display = 'none'
    document.getElementsByClassName('start1')[0].style.display = 'none'
    document.getElementsByClassName('tap')[0].style.display = 'block'
    document.getElementsByClassName('noofclicks')[0].style.display = 'flex'
    document.getElementsByTagName('p')[0].style.display = 'flex'
    document.getElementsByTagName('p')[0].style.alignItems = 'center'
    document.getElementsByTagName('p')[0].style.justifyContent = 'center'
  }
})



document.getElementsByClassName('restart')[0].addEventListener('click',(event)=>{
  document.getElementsByClassName('restart1')[0].style.display = 'flex'
})

document.getElementsByClassName('playagain')[0].addEventListener('click',(event)=>{
  cardfliped.forEach((event)=>{
    event.target.style.backgroundImage = 'url("./gifs/card.jpeg")'
  })
  document.getElementsByClassName('restart')[0].style.display = 'none'
  document.getElementsByClassName('tap')[0].style.display = 'block'
  document.getElementsByClassName('gameover')[0].style.display = 'none'
  document.getElementsByClassName('restart1')[0].style.display = 'none'
  lis = []
  noofclicks = 0
  nooftiles = 0;
  cardfliped = []
  document.querySelector('.noofclicks').innerText = `Bestscore = ${bestscore} Score = ${noofclicks} cards flip = ${nooftiles}`
  deleteDivsForColor()
  setTimeout(()=>{
    createDivsForColors(shufflegif)
  },100)
})

document.getElementsByClassName('playagain')[1].addEventListener('click',(event)=>{
  cardfliped.forEach((event)=>{
    event.target.style.backgroundImage = 'url("./gifs/card.jpeg")'
  })
  document.getElementsByClassName('restart')[0].style.display = 'none'
  document.getElementsByClassName('tap')[0].style.display = 'block'
  document.getElementsByClassName('gameover')[0].style.display = 'none'
  document.getElementsByClassName('restart1')[0].style.display = 'none'
  lis = []
  noofclicks = 0
  nooftiles = 0;
  cardfliped = []
  document.querySelector('.noofclicks').innerText = `Bestscore = ${bestscore} Score = ${noofclicks} cards flip = ${nooftiles}`
  deleteDivsForColor()
  setTimeout(()=>{
    createDivsForColors(shufflegif)
  },100)
})

document.getElementsByClassName('no')[0].addEventListener('click',(event)=>{
  document.getElementsByClassName('gameover')[0].style.display = 'none'
  document.getElementsByClassName('restart1')[0].style.display = 'none'
  
})

document.getElementsByClassName('no')[1].addEventListener('click',(event)=>{
  document.getElementsByClassName('gameover')[0].style.display = 'none'
  document.getElementsByClassName('restart1')[0].style.display = 'none'
  
})